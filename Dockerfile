#########################
# Create base container #
#########################
FROM ubuntu:20.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=mailscanner
ENV APP_GROUP=mailscanner

# Set version pins
ENV VERSION="5.4.5-3"
ENV MAILWATCH_VERSION="1.2.19"
ENV DOWNLOAD_URL="https://github.com/MailScanner/v5/releases/download/${VERSION}/MailScanner-${VERSION}.noarch.deb"


# Set base dependencies
ENV BASE_DEPS ca-certificates \
              clamav \
              clamav-daemon \
              cpanminus \
              gcc \
              libc6-dev \
              libclamav-client-perl \
              libdata-ieee754-perl \
              libdata-printer-perl \
              libdata-validate-ip-perl \
              libdbd-mysql-perl \
              libencoding-fixlatin-perl \
              libio-string-perl \
              liblist-allutils-perl \
              libmaxmind-db-reader-perl \
              libmaxmind-db-reader-xs-perl \
              libnet-works-perl \
              make \
              mysql-client \
              perl \
              spamassassin \
              tzdata \
              wget\
              msmtp-mta \
              nginx \
              php-curl \
              php-fpm \
              php-gd \
              php-imap \
              php-ldap \
              php-mbstring \
              php-mysql \
              php-xml \
              python3 \
              python3-pip

# Set build dependencies
ENV BUILD_DEPS build-essential

# Create app user and group
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -r -m -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_DEPS} \
 && cpanm --notest Mail::SPF::Query Digest::SHA1 IP::Country IP::Country::DB_File \
 && wget -nv ${DOWNLOAD_URL} -O /root/MailScanner-${VERSION}.noarch.deb \
 && dpkg -i /root/MailScanner-${VERSION}.noarch.deb \
 && /usr/sbin/ms-configure --MTA=none --installClamav=N --installCPAN=N --ignoreDeps=N --ramdiskSize=0 \
 && rm /root/MailScanner-${VERSION}.noarch.deb \
 && pip3 install yacron \
 && dpkg --purge cpanminus make \
 && rm -rf /root/.cpanm \
 && apt-get autoremove -y \
 && rm /var/lib/clamav/* \
 && rm -Rf /root/.cpanm \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS} \
 && rm -rf /var/lib/apt/lists/*

# Run build
COPY build.sh /usr/src/build.sh
WORKDIR /usr/src
RUN ./build.sh

##########################
# Create final container #
##########################
FROM base

# Finalize install
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV MW_DB_USER='mailscanner'
ENV MW_DB_PORT='3306'
ENV MW_DB_PASS=''
ENV MW_DB_HOST=''
ENV MW_DB_NAME='mailscanner'
ENV MW_USE_LDAP='false'
ENV MW_LDAP_HOST=''
ENV MW_LDAP_PORT='389'
ENV MW_LDAP_DN=''
ENV MW_LDAP_USER=''
ENV MW_LDAP_PASS=''
ENV MW_LDAP_FILTER='(uid=%s)'
ENV MW_LDAP_EMAIL_FIELD='mail'
ENV MW_LDAP_USERNAME_FIELD='uid'
ENV MW_LDAP_BIND_PREFIX='uid='
ENV MW_LDAP_BIND_SUFFIX=''
ENV MW_USE_IMAP='false'
ENV MW_IMAP_HOST=''
ENV MW_IMAP_AUTOCREATE_VALID_USER='false'
ENV MW_IMAP_USERNAME_FULL_EMAIL='true'
ENV MW_TIME_ZONE='America/Los_Angeles'
ENV MW_SMTP_HOST=''
ENV MW_SMTP_PORT='10025'
ENV MW_SMTP_FROM=''
ENV MW_HOST_URL=''

RUN update-ca-certificates \
 && chown -R ${APP_USER}:${APP_GROUP} /var/spool/MailScanner \
 && chown -R ${APP_USER}:${APP_GROUP} /var/lib/spamassassin \
 && mkdir -p /var/run/MailScanner \
 && chown -R ${APP_USER}:${APP_GROUP} /var/run/MailScanner \
 && mkdir -p /var/run/clamav \
 && chown -R ${APP_USER}:${APP_GROUP} /var/run/clamav \
 && cp /usr/local/share/mailwatch/tools/Cron_jobs/mailwatch /etc/cron.daily/ \
 && ln -s /usr/local/share/mailwatch/MailScanner_perl_scripts/MailWatch.pm /usr/share/MailScanner/perl/custom \
 && ln -s /usr/local/share/mailwatch/MailScanner_perl_scripts/MailWatchConf.pm /usr/share/MailScanner/perl/custom \
 && ln -s /usr/local/share/mailwatch/MailScanner_perl_scripts/SQLBlackWhiteList.pm /usr/share/MailScanner/perl/custom \
 && ln -s /usr/local/share/mailwatch/MailScanner_perl_scripts/SQLSpamSettings.pm /usr/share/MailScanner/perl/custom \
 && ln -sf /dev/stdout /var/log/nginx/access.log \
 && ln -sf /dev/stderr /var/log/nginx/error.log

# Prepare container
VOLUME ["/etc/MailScanner", "/var/spool/MailScanner"]
USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["mailscanner"]
