#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    mkdir -p /var/spool/MailScanner/{milterout,quarantine,milterin,incoming,archive,spamassassin,clamav}
    freshclam
    mysql --user="${MW_DB_USER}" --database="${MW_DB_NAME}" --password="${MW_DB_PASS}" --host="${MW_DB_HOST}" < /usr/local/share/mailwatch/create.sql
    mysql --user="${MW_DB_USER}" --database="${MW_DB_NAME}" --password="${MW_DB_PASS}" --host="${MW_DB_HOST}" --execute "INSERT INTO users SET username = 'mwadmin', password = MD5('${MW_ADMIN_PASS}'), fullname = 'Mailwatch Admin', type = 'A' ON DUPLICATE KEY UPDATE password = MD5('${MW_ADMIN_PASS}')"
    exec echo "MailScanner components initalized."
fi

if [ "$1" = 'cron' ]; then
    exec yacron
fi

if [ "$1" = 'clamav' ]; then
    exec clamd
fi

if [ "$1" = 'mailscanner' ]; then
    exec /usr/sbin/MailScanner
fi

if [ "$1" = 'msmilter' ]; then
    exec /usr/sbin/MSMilter
fi

if [ "$1" = 'nginx' ]; then
    exec nginx -g "daemon off;"
fi

if [ "$1" = 'php-fpm' ]; then
    exec php-fpm7.4 -F
fi

exec "$@"
