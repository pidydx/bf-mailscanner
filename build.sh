#!/bin/bash

set -e

wget -nv https://github.com/mailwatch/MailWatch/archive/v${MAILWATCH_VERSION}.tar.gz
mkdir -p /usr/local/share/mailwatch
tar -zxf v${MAILWATCH_VERSION}.tar.gz --strip-components 1 -C /usr/local/share/mailwatch

sed -i -e 's/\$pathToFunctions = .*/\$pathToFunctions = \/usr\/local\/share\/mailwatch\/mailscanner\/functions.php/' /usr/local/share/mailwatch/tools/Cron_jobs/*.php
cp /usr/local/share/mailwatch/tools/Cron_jobs/mailwatch_db_clean.php /usr/local/bin/
cp /usr/local/share/mailwatch/tools/Cron_jobs/mailwatch_quarantine_maint.php /usr/local/bin/
cp /usr/local/share/mailwatch/tools/Cron_jobs/mailwatch_quarantine_report.php /usr/local/bin/
